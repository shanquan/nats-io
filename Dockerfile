FROM golang
WORKDIR /go
RUN mkdir -p src/github.com/nats-io && cd src/github.com/nats-io
RUN git clone https://github.com/nats-io/nats.go.git
RUN go get github.com/nats-io/nats.go/
WORKDIR /go/src/github.com/nats-io/nats.go
CMD bash
